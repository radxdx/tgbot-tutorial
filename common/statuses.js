// Статусы для state-machine (fsm)

module.exports = {
  greeting: 'greeting',
  cmd1: 'cmd1',
  cmd2: 'cmd2',
  kb1: 'kb1',
  kb2: 'kb2',
  kb3: 'kb3',
  ikb1: 'ikb1',
  ikb2: 'ikb2',
  ikb3: 'ikb3',
  invalidInput: 'invalidInput'
};
