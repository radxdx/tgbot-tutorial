// Маппинг кодов действий для команд, кнопок и callback_query с инлайновых клавиатур;
const btn = require('../client/buttons');
const cmd = require('../client/commands');

module.exports = {
  customInput: '01',
  invalidInput: '02',
  [btn.toStart]: '03',
  [cmd.start]: '04',
  [cmd.cmd1]: '05',
  [cmd.cmd2]: '06',
  [btn.toScreen1]: '07',
  [btn.toScreen2]: '08',
  [btn.toScreen3]: '09',
  [btn.toInline1]: '10',
  ikb1toikb3: '11',
  ikb2tokb2: '12',
  ikb3tokb1: '13'
};
