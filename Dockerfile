# ---- Base Node ----
FROM node:10.17.0-alpine3.10 AS base
WORKDIR /tgbot-template
COPY package*.json ./

# ---- Prod-dependencies ----
FROM base AS dependencies
RUN npm install --only=production
# Рекурсивно копирует модули в отдельную папку prod-модулей
RUN cp -R node_modules prod_node_modules

# ---- Release ----
FROM base AS release
# Копирует файлы приложения из корневой директории
COPY . ./
# Копирует чистые prod-модули
COPY --from=dependencies /tgbot-template/prod_node_modules ./node_modules
ARG NODE_ENV=prod
ENV NODE_ENV="${NODE_ENV}"

EXPOSE 8000

ENTRYPOINT ["node"]
CMD ["bot.js"]
