module.exports = {
  'env': {
    'es2021': true,
    'node': true
  },
  'extends': 'eslint:recommended',
  'overrides': [
    {
      'env': {
        'node': true
      },
      'files': [
        '.eslintrc.{js,cjs}'
      ],
      'parserOptions': {
        'sourceType': 'script'
      }
    }
  ],
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module'
  },
  'rules': {
    'indent': [
      'error',
      2
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'semi': [
      'error',
      'always'
    ],
    'curly': ['error', 'all'],
    'dot-notation': 'error',
    'arrow-parens': 'error',
    'arrow-spacing': 'error',
    'no-const-assign': 'error',
    'no-class-assign': 'error',
    'no-multi-spaces': ['error'],
    'comma-dangle': ['error', 'never'],
    'quotes': [
      'error',
      'single',
      {
        avoidEscape: true,
        allowTemplateLiterals: true
      }
    ],
    'no-unused-vars': [
      'error',
      {
        vars: 'all',
        args: 'after-used',
        argsIgnorePattern: 'next'
      }
    ],
    'eol-last': ['error', 'unix'],
    'no-path-concat': 'error',
    'no-new-symbol': 'error',
    'template-curly-spacing': ['error', 'never'],
    'prefer-template': 'error',
    'no-array-constructor': 'error',
    'no-shadow': 'error',
    'no-var': 'error',
    'no-trailing-spaces': 'error',
    'no-else-return': 'error',
    'use-isnan': 'error',
    'no-cond-assign': ['error', 'except-parens'],
    'array-bracket-newline': ['error', 'consistent'],
    'keyword-spacing': 'error',
    'object-curly-spacing': ['error', 'always'],
    'space-in-parens': ['warn', 'never'],
    'no-use-before-define': [
      'warn',
      {
        functions: false,
        classes: true
      }
    ],
    'no-duplicate-imports': [
      'error',
      {
        includeExports: true
      }
    ],
    'prefer-arrow-callback': [
      'error',
      {
        allowNamedFunctions: true
      }
    ],
    'require-jsdoc': [
      'error',
      {
        require: {
          FunctionDeclaration: true,
          MethodDefinition: false,
          ClassDeclaration: false
        }
      }
    ],
    'valid-jsdoc': [
      'error',
      {
        requireReturn: false,
        prefer: {
          return: 'returns'
        }
      }
    ],
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'never',
        named: 'never',
        asyncArrow: 'always'
      }
    ],
    'max-len': [
      'warn',
      {
        code: 120
      }
    ],
    'no-multiple-empty-lines': ['error', { max: 1 }]
  }
};
