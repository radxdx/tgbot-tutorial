const config = require('config');
const { Telegraf } = require('telegraf');

const Database = require('./server/db/database');
const Api = require('./server/api');

class Bot {
  constructor() {
    this.core = new Telegraf(config.botApiToken);
    this.db = new Database(config.db.databaseUrl);
    this.api = new Api(this.core, this.db);
  }

  async up() {
    // database setup
    await this.db.setupDatabase()
      .then(() => console.log('Database connected'))
      .catch((err) => console.log(`DB Error ${err}`));

    // setup api listeners
    this.api.setup();
  }

  async run() {
    await this.up();
    console.log('Bot has been started');
  }
}

const bot = new Bot;
bot.run();

// нужны для корректного выполнения ботом команд в облачных сервисах
process.once('SIGINT', () => bot.core.stop('SIGINT'));
process.once('SIGTERM', () => bot.core.stop('SIGTERM'));
