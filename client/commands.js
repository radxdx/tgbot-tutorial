const start = '/start';
const cmd1 = '/cmd1';
const cmd2 = '/cmd2';

module.exports = {
  start,
  cmd1,
  cmd2
};
