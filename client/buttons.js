const toStart = 'В начало';
const toScreen1 = 'К экрану 1';
const toScreen2 = 'К экрану 2';
const toScreen3 = 'К экрану 3';
const toInline1 = 'К инлайну 1';

const toInline3 = {
  text: 'К инлайну 3',
  callback_data: JSON.stringify({
    act: '11'
  })
};
const toScreen2viaIkb2 = {
  text: 'К экрану 2',
  callback_data: JSON.stringify({
    act: '12'
  })
};
const toScreen1viaIkb3 = {
  text: 'К экрану 1',
  callback_data: JSON.stringify({
    act: '13'
  })
};

module.exports = {
  toStart,
  toScreen1,
  toScreen2,
  toScreen3,
  toInline1,
  toInline3,
  toScreen2viaIkb2,
  toScreen1viaIkb3
};
