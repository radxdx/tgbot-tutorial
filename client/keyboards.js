const btn = require('./buttons');

module.exports = {
  greeting: [
    [btn.toScreen1]
  ],
  kb1: [
    [btn.toScreen2, btn.toScreen3]
  ],
  kb2: [
    [btn.toInline1]
  ],
  kb3: [
    [btn.toScreen1]
  ],
  ikb1: [
    [btn.toInline3]
  ],
  ikb2: [
    [btn.toScreen2viaIkb2]
  ],
  ikb3: [
    [btn.toScreen1viaIkb3]
  ],
  invalidInput: [
    [btn.toStart]
  ]
};
