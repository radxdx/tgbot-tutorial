const { State } = require('../../server/fsm');
const statuses = require('../../common/statuses');
const actions = require('../../common/actions');
const btn = require('../../client/buttons');
const cmd = require('../../client/commands');
let state;

beforeAll(() => {
  state = new State();
});

describe('FSM healthcheck', () => {
  test('Testing FSM initialization', () => {
    expect(state).toBeTruthy();
  });
});

// Проверка возможности переходов
describe('Commands transitions', () => {
  test('Command start', () => {
    expect(state.can(actions[cmd.start])).toBeTruthy();
  });
  test('Command cmd1', () => {
    expect(state.can(actions[cmd.cmd1])).toBeTruthy();
  });
  test('Command cmd2', () => {
    expect(state.can(actions[cmd.cmd2])).toBeTruthy();
  });
});

describe('KB transitions', () => {
  test('toScreen1', () => {
    expect(state.can(actions[btn.toScreen1])).toBeTruthy();
  });
  test('toScreen2', () => {
    state.goto(statuses.kb1)
    expect(state.can(actions[btn.toScreen2])).toBeTruthy();
  });
  test('toScreen3', () => {
    state.goto(statuses.kb1)
    expect(state.can(actions[btn.toScreen3])).toBeTruthy();
  });
});

describe('IKB transitions', () => {
  test('toIKB1', () => {
    state.goto(statuses.kb2)
    expect(state.can(actions[btn.toInline1])).toBeTruthy();
  });
  test('toIKB2', () => {
    state.goto(statuses.ikb1)
    expect(state.can(actions.customInput)).toBeTruthy();
  });
  test('toScreen2', () => {
    state.goto(statuses.ikb2)
    expect(state.can(actions.ikb2tokb2)).toBeTruthy();
  });
  test('toIKB3', () => {
    state.goto(statuses.ikb1)
    expect(state.can(actions.ikb1toikb3)).toBeTruthy();
  });
  test('toScreen1', () => {
    state.goto(statuses.ikb3)
    expect(state.can(actions.ikb3tokb1)).toBeTruthy();
  });
});

describe('Common transitions', () => {
  test('toStart', () => {
    expect(state.can(actions[btn.toStart])).toBeTruthy();
  });
  test('customInput', () => {
    expect(state.can(actions.customInput)).toBeTruthy();
  });
  test('invalidInput', () => {
    expect(state.can(actions.invalidInput)).toBeTruthy();
  });
});

describe('Whole flow', () => {
  test('Command start', () => {
    state[actions[cmd.start]]()
    expect(state.state).toBe(statuses.greeting);
  });
  test('Command cmd1', () => {
    state[actions[cmd.cmd1]]()
    expect(state.state).toBe(statuses.cmd1);
  });
  test('Command cmd2', () => {
    state[actions[cmd.cmd2]]()
    expect(state.state).toBe(statuses.cmd2);
  });
  test('toScreen1', () => {
    state[actions[btn.toScreen1]]()
    expect(state.state).toBe(statuses.kb1);
  });
  test('toScreen3', () => {
    state[actions[btn.toScreen3]]()
    expect(state.state).toBe(statuses.kb3);
  });
  test('back toScreen1', () => {
    state[actions[btn.toScreen1]]()
    expect(state.state).toBe(statuses.kb1);
  });
  test('toScreen2', () => {
    state[actions[btn.toScreen2]]()
    expect(state.state).toBe(statuses.kb2);
  });
  test('toIKB1', () => {
    state[actions[btn.toInline1]]()
    expect(state.state).toBe(statuses.ikb1);
  });
  test('toIKB2', () => {
    state[actions.customInput]()
    expect(state.state).toBe(statuses.ikb2);
  });
  test('toKB2', () => {
    state[actions.ikb2tokb2]()
    expect(state.state).toBe(statuses.kb2);
  });
  test('again toIKB1', () => {
    state[actions[btn.toInline1]]()
    expect(state.state).toBe(statuses.ikb1);
  });
  test('toIKB3', () => {
    state[actions.ikb1toikb3]()
    expect(state.state).toBe(statuses.ikb3);
  });
  test('back toKB1', () => {
    state[actions.ikb3tokb1]()
    expect(state.state).toBe(statuses.kb1);
  });
  test('custom input', () => {
    state[actions.customInput]()
    expect(state.state).toBe(statuses.invalidInput);
  });
  test('invalid input', () => {
    state[actions.invalidInput]()
    expect(state.state).toBe(statuses.invalidInput);
  });
  test('back to start', () => {
    state[actions[btn.toStart]]()
    expect(state.state).toBe(statuses.kb1);
  });
});
