const { ikb2 } = require('../../server/handlers');
const controllers = require('../../server/controllers');
let botMock, dbMock, sessionMock, ctxMain, ctxEdge;
let sumMain, isWrongSumMain, sumEdge, isWrongSumEdge;
let invalidInputMock, ikb2Mock, originalInvalidInput, originalIkb2;

describe('Testing handlers (controller managers)', () => {
  beforeAll(() => {
    // Создаем моки для аргументов
    botMock = jest.fn();
    dbMock = {
      orm: {
        transaction: jest.fn() // Мокируем метод transaction
      },
      models: {
        TGState: {
          findOne: jest.fn(),
          update: jest.fn()
        }
      }
    };
    sessionMock = {
      goto: jest.fn()
    };

    // Передаваемые контексты
    ctxMain = {
      from: {
        id: 1
      },
      message: {
        text: 100
      },
      reply: jest.fn() // Создание мок-функции для reply
    };
    sumMain = parseFloat(ctxMain.message.text); // вернет число
    isWrongSumMain = Number.isNaN(sumMain);

    ctxEdge = {
      from: {
        id: 1
      },
      message: {
        text: 'edge'
      },
      reply: jest.fn() // Создание мок-функции для reply
    };
    sumEdge = parseFloat(ctxEdge.message.text); // вернет NaN
    isWrongSumEdge = Number.isNaN(sumEdge);

    // Создаем мок для функций controllers.invalidInput и controllers.ikb2
    invalidInputMock = jest.fn();
    ikb2Mock = jest.fn();

    // Сохраняем оригинальные функции
    originalInvalidInput = controllers.invalidInput;
    originalIkb2 = controllers.ikb2;

    // Заменяем оригинальные функции на моки
    controllers.invalidInput = invalidInputMock;
    controllers.ikb2 = ikb2Mock;
  });

  afterAll(() => {
    // Возвращаем оригинальные функции обратно
    controllers.invalidInput = originalInvalidInput;
    controllers.ikb2 = originalIkb2;
  });

  test('Testing IKB2 handler - main case', async () => {
    await ikb2(botMock, dbMock, sessionMock, ctxMain);

    // Проверяем, были ли вызваны ожидаемые функции
    if (isWrongSumMain) {
      expect(invalidInputMock).toHaveBeenCalled();
    } else {
      expect(ikb2Mock).toHaveBeenCalled();
    }
  });

  test('Testing IKB2 handler - edge case', async () => {
    await ikb2(botMock, dbMock, sessionMock, ctxEdge);

    // Проверяем, были ли вызваны ожидаемые функции
    if (isWrongSumEdge) {
      expect(invalidInputMock).toHaveBeenCalled();
    } else {
      expect(ikb2Mock).toHaveBeenCalled();
    }
  });
});
