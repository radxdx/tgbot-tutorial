module.exports = {
  db: {
    dialect: 'postgres',
    schema: 'public',
    logging: false
  }
};
