require('dotenv').config();

module.exports = {
  botApiToken: process.env.BOT_API_TOKEN,
  db: {
    databaseUrl: process.env.DATABASE_URL,
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    host: process.env.DATABASE_HOST,
    port: 5432,
    dialectOptions: {}
  }
};
