const config = require('config');

module.exports = {
  'dev-local': config.db,
  'dev': config.db,
  'prod': config.db
};
