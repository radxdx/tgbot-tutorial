const { SessionManager } = require('./sessions');
const { updateState } = require('./db/queries/update-state');

class Api {
  constructor(bot, database) {
    this.core = bot;
    this.db = database;
    this.userSessions = new SessionManager(this.db);
  }

  async handleBotEvent(ctx) {
    const session = await this.userSessions.ensure(ctx);
    const [controller, state] = session.chooseCB(ctx);

    await updateState(this.db, ctx.from.id, state);
    await controller(this.core, this.db, session, ctx);
  }

  setup() {

    // bot action on event 'message'
    this.core.on('message', async (ctx) => {
      if (ctx.callbackQuery) { return; }

      await this.handleBotEvent(ctx);
    });

    // bot action on event 'callback_query'
    this.core.on('callback_query', async (ctx) => {
      await this.handleBotEvent(ctx);
    });

    // bot error handling
    this.core.catch((err, ctx) => {
      console.log(`Блять, вылетела ошибка: ${ctx.updateType}`, err);
    });

    this.core.launch();

    console.log('API connected');
  }
}

module.exports = Api;
