const EventEmitter = require('events');

const { State } = require('./fsm');
const routes = require('./routes');
const actions = require('../common/actions');
const statuses = require('../common/statuses');

const { getOrCreateUser } = require('./db/queries/get-or-create-user');
const { getOrCreateState } = require('./db/queries/get-or-create-state');
const { updateState } = require('./db/queries/update-state');

class SessionManager {
  constructor(database) {
    this.db = database;
    this.sessions = {};
    this.sessionDropper(); // мониторит и сбрасывает истекшие сессии
  }

  async ensure(ctx) { // гарантирует сессию для пользователя
    const userID = ctx.from.id;

    if (this.sessions[userID] == null) {
      this.sessions[userID] = new Session(userID);

      try {
        // запись пользователя в базу, если его там еще нет
        const tguser = await getOrCreateUser(this.db, ctx);
        // получение состояния пользователя из базы / инициализация состояния
        const tgstate = await getOrCreateState(this.db, tguser);
        // открытие текущей сессии с последнего сохраненного состояния
        this.sessions[userID].goto(tgstate.status);

      } catch (err) {
        console.log('ensure failed: ', err);
      }
    }

    return this.sessions[userID];
  }

  sessionDropper() {

    this.timerId = setInterval(async () => {
      for (let userID in this.sessions) {
        if (this.sessions[userID].isExpired()) {
          try { // сохранение состояния в БД
            const state = this.sessions[userID].state.state;
            await updateState(this.db, userID, state);

          } catch (err) {
            console.log('saving session failed: ', err);
          }

          delete this.sessions[userID];
        }
      }
    }, 600000); // 1000 * 60 * 10 = 10 минут
  }
}

class Session extends EventEmitter {
  constructor(userID) {
    super();
    this.userID = userID;
    this.state = new State();
    this.lastOperationAt = Date.now();
  }

  goto(status) {
    this.state.goto(status);
  }

  isExpired() {
    const elapsedMinutes = (Date.now() - this.lastOperationAt) / (1000 * 60);
    return elapsedMinutes > 10; // сессия истекает через 10 минут
  }

  chooseCB(ctx) {
    this.lastOperationAt = Date.now();

    if (ctx.callbackQuery != null) {
      return this.chooseCBOnInline(ctx.callbackQuery);
    }
    return this.chooseCBOnMessage(ctx.message);

  }

  chooseCBOnInline(cbQuery) {
    const commandName = JSON.parse(cbQuery.data).act;
    const commandClear = this.state.can(commandName);

    if (!commandClear) { // если введена кастомная строка
      try {
        this.state[actions.customInput](); // переход к стейту ***handling

      } catch (error) { // если состояние не предполагает кастомной строки
        console.log(error);
        this.state[actions.invalidInput](); // к стейту invalidInput
      }
    }

    if (commandClear) { // если введена существующая команда
      this.state[commandName](); // переход к новому стейту
    }

    const cb = routes[this.state.state] || routes[statuses.invalidInput];
    const state = this.state.state;
    return [cb, state];
  }

  chooseCBOnMessage(msg) { // определяет контролера для обработки команды
    const commandName = actions[msg.text];
    const commandClear = this.state.can(commandName);

    if (!commandClear) { // если введена кастомная строка
      try {
        this.state[actions.customInput](); // переход к стейту ***handling

      } catch (error) { // если состояние не предполагает кастомной строки
        console.log(error);
        this.state[actions.invalidInput](); // к стейту invalidInput
      }
    }

    if (commandClear) { // если введена существующая команда
      this.state[commandName](); // переход к новому стейту
    }

    const cb = routes[this.state.state] || routes[statuses.invalidInput];
    const state = this.state.state;
    return [cb, state];
  }
}

module.exports = {
  SessionManager
};
