// Промежуточные обработчики кастомных строк
const statuses = require('../common/statuses');
const controllers = require('./controllers');

const { updateState } = require('./db/queries/update-state');

const ikb2 = async (bot, db, session, ctx) => {
  const sum = parseFloat(ctx.message.text); // вернет число либо NaN
  const isWrongSum = Number.isNaN(sum);
  let cb = undefined;

  if (isWrongSum) {
    try {
      session.goto(statuses.invalidInput); // к стейту ***wrong
      await updateState(db, ctx.from.id, statuses.invalidInput);
      cb = controllers.invalidInput;
    } catch (error) {
      console.log(error);
    }
  }

  if (!isWrongSum) {
    try {
      session.goto(statuses.ikb2); // к стейту ***confirmation
      await updateState(db, ctx.from.id, statuses.ikb2);
      cb = controllers.ikb2;
    } catch (error) {
      console.log(error);
      session.goto(statuses.invalidInput); // к стейту invalidInput
      await updateState(db, ctx.from.id, statuses.invalidInput);
      cb = controllers.invalidInput;
    }
  }

  await cb(bot, db, session, ctx);
};

module.exports = {
  ikb2
};
