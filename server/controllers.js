// Реализация бизнес-логики, отправка сообщений и клавиатур пользователю,
// ведение логов.
const keyboards = require('../client/keyboards');

const greeting = async (bot, db, session, ctx) => {
  const text = `Здравствуйте, ${ctx.from.first_name}!\n` +
    `Это страница приветствия`;

  ctx.reply(text, {
    reply_markup: {
      keyboard: keyboards.greeting,
      resize_keyboard: true
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} начал работу`);
};

const cmd1 = async (bot, db, session, ctx) => {
  const text = 'Страница 1 команды';

  ctx.reply(text, {
    reply_markup: {
      keyboard: keyboards.greeting,
      resize_keyboard: true
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} начал работу`);
};

const cmd2 = async (bot, db, session, ctx) => {
  const text = 'Страница 2 команды';

  ctx.reply(text, {
    reply_markup: {
      keyboard: keyboards.greeting,
      resize_keyboard: true
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} начал работу`);
};

const kb1 = async (bot, db, session, ctx) => {
  if (ctx.callbackQuery) {
    ctx.answerCbQuery(); // завершает обработку предыдущего коллбека, если пришли с инлайна
    ctx.deleteMessage(); // удаляет предыдущее сообщение с инлайн-клавой
  }

  const text = 'Страница 1 клавы. Основная';

  ctx.reply(text, {
    reply_markup: {
      keyboard: keyboards.kb1,
      resize_keyboard: true
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} на странице 1 клавы, основной`);
};

const kb2 = async (bot, db, session, ctx) => {
  if (ctx.callbackQuery) {
    ctx.answerCbQuery(); // завершает обработку предыдущего коллбека, если пришли с инлайна
    ctx.deleteMessage(); // удаляет предыдущее сообщение с инлайн-клавой
  }

  const text = 'Страница 2 клавы';

  ctx.reply(text, {
    reply_markup: {
      keyboard: keyboards.kb2,
      resize_keyboard: true,
      one_time_keyboard: true
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} на странице 2 клавы`);
};

const kb3 = async (bot, db, session, ctx) => {
  const text = 'Страница 3 клавы';

  ctx.reply(text, {
    reply_markup: {
      keyboard: keyboards.kb3,
      resize_keyboard: true
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} на странице 3 клавы`);
};

const ikb1 = async (bot, db, session, ctx) => {
  const text = `Страница 1 инлайн клавы\n` +
    `Введите число или перейдите на инлайн 3`;

  ctx.reply(text, {
    reply_markup: {
      inline_keyboard: keyboards.ikb1,
      remove_keyboard: true // удаляет предыдущую обычную клаву
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} на странице 1 инлайн клавы`);
};

const ikb2 = async (bot, db, session, ctx) => {
  if (ctx.callbackQuery) {
    ctx.answerCbQuery(); // завершает обработку предыдущего коллбека, если пришли с инлайна
    ctx.deleteMessage(); // удаляет предыдущее сообщение с инлайн-клавой
  }

  const text = 'Страница 2 инлайн клавы';

  ctx.reply(text, {
    reply_markup: {
      inline_keyboard: keyboards.ikb2
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} на странице 2 инлайн клавы`);
};

const ikb3 = async (bot, db, session, ctx) => {
  const text = 'Страница 3 инлайн клавы';

  // ctx.editMessageReplyMarkup(); // редактирует предыдущее сообщение
  ctx.deleteMessage(); // удаляет предыдущее сообщение с инлайн-клавой
  ctx.answerCbQuery(); // завершает обработку предыдущего коллбека
  ctx.reply(text, {
    reply_markup: {
      inline_keyboard: keyboards.ikb3
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} на странице 3 инлайн клавы`);
};

const invalidInput = async (bot, db, session, ctx) => {
  const text = `Не могу понять команду.\n` +
    `Давайте начнем сначала.`;

  ctx.reply(text, {
    reply_markup: {
      keyboard: keyboards.invalidInput,
      resize_keyboard: true
    }
  });

  console.log(`${ctx.from.first_name} ${ctx.from.last_name} ` +
    `ввел какую-то дичь`);
};

module.exports = {
  greeting,
  cmd1,
  cmd2,
  kb1,
  kb2,
  kb3,
  ikb1,
  ikb2,
  ikb3,
  invalidInput
};
