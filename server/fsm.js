const StateMachine = require('javascript-state-machine');
const statuses = require('../common/statuses');
const actions = require('../common/actions');
const btn = require('../client/buttons');
const cmd = require('../client/commands');

const State = StateMachine.factory({
  init: statuses.greeting,
  transitions: [

    // команды бота
    {
      name: actions[cmd.start],
      from: '*',
      to: statuses.greeting // -> приветственная страница
    },
    {
      name: actions[cmd.cmd1],
      from: '*',
      to: statuses.cmd1 // -> страница команды 1
    },
    {
      name: actions[cmd.cmd2],
      from: '*',
      to: statuses.cmd2 // ->  страница команды 1
    },

    // команды клавиатур
    {
      name: actions[btn.toScreen1],
      from: [ statuses.greeting, statuses.kb3, statuses.ikb3, statuses.cmd1, statuses.cmd2],
      to: statuses.kb1 // -> главная страница
    },
    {
      name: actions[btn.toScreen2],
      from: [ statuses.kb1, statuses.ikb2 ],
      to: statuses.kb2 // -> страница 2 экрана
    },
    {
      name: actions[btn.toScreen3],
      from: [ statuses.kb1 ],
      to: statuses.kb3 // -> страница 3 экрана
    },

    // команды инлайн-клавиатур
    {
      name: actions[btn.toInline1],
      from: [ statuses.kb2 ],
      to: statuses.ikb1 // -> первая инлайн
    },
    {
      name: actions.customInput,
      from: [ statuses.ikb1 ],
      to: statuses.ikb2 // -> страница 2 инлайна
    },
    {
      name: actions.ikb2tokb2,
      from: [ statuses.ikb2 ],
      to: statuses.kb2 // -> страница 2 клавы
    },
    {
      name: actions.ikb1toikb3,
      from: [ statuses.ikb1 ],
      to: statuses.ikb3 // -> страница 3 инлайна
    },
    {
      name: actions.ikb3tokb1,
      from: [ statuses.ikb3 ],
      to: statuses.kb1 // -> к основной странице
    },

    // общие переходы
    {
      name: actions[btn.toStart],
      from: '*',
      to: statuses.kb1 // -> главная страница
    },
    {
      name: actions.customInput,
      from: '*',
      to: statuses.invalidInput
    },
    {
      name: actions.invalidInput,
      from: '*',
      to: statuses.invalidInput
    },
    {
      name: 'goto',
      from: '*',
      to: function(s) { return s; } // директивный переход к любому стейту
    }
  ]
});

module.exports = {
  State
};
