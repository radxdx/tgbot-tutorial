'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable('tgstates', {
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          references: {
            model: 'tgusers',
            key: 'id'
          },
          field: 'user_id'
        },
        status: {
          type: Sequelize.TEXT,
        },
        payload: {
          type: Sequelize.TEXT,
        }
      }, { transaction });

      await transaction.commit();

    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  down: async (queryInterface, Sequelize) => {

    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('tgstates', { transaction });
      await transaction.commit();

    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }
};
