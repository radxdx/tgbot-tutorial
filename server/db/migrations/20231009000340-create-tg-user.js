'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    const { Op } = Sequelize;

    try {
      await queryInterface.createTable('tgusers', {
        id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true,
          primaryKey: true
        },
        firstName: {
          type: Sequelize.TEXT,
          field: 'first_name'
        },
        lastName: {
          type: Sequelize.TEXT,
          field: 'last_name'
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.literal('current_timestamp'),
          field: 'created_at'
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.literal('current_timestamp'),
          field: 'updated_at'
        }
      }, { transaction });

      await queryInterface.addConstraint('tgusers', {
        fields: ['first_name'],
        type: 'check',
        where: {
          firstName: Sequelize.where(
            Sequelize.fn('char_length', Sequelize.col('first_name')),
            { [Op.lt]: 33 },
          ),
        },
        transaction,
      });

      await queryInterface.addConstraint('tgusers', {
        fields: ['last_name'],
        type: 'check',
        where: {
          lastName: Sequelize.where(Sequelize.fn('char_length', Sequelize.col('last_name')), {
            [Op.lt]: 33
          })
        },
        transaction,
      });

      // seeding admins
      await queryInterface.bulkInsert('tgusers', [
        { id: 269008927, 'first_name': 'Ruslan', 'last_name': 'Tuktarov' },
        { id: 109345649, 'first_name': 'Sergey', 'last_name': 'Stryukov' },
      ], { transaction });

      await transaction.commit();

    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  down: async (queryInterface, Sequelize) => {

    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('tgusers', { transaction });
      await transaction.commit();

    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  }
};
