'use strict';

const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;
const Model = Sequelize.Model;

const TGUser = require('./tguser');

class TGState extends Model {
  static init(sequelize) {
    return super.init({
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        field: 'user_id',
        references: {
          model: TGUser,
          key: 'id'
        },
      },
      status: {
        type: DataTypes.TEXT
      },
      payload: {
        type: DataTypes.TEXT
      }
    }, {
      sequelize,
      modelName: 'TGState',
      tableName: 'tgstates',
      timestamps: false
    });
  }

  static associate(models) {
    models.TGState.belongsTo(models.TGUser, {
      foreignKey: 'user_id' // column in source table
    });
  }
};

module.exports = TGState;
