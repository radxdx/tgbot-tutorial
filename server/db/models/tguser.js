'use strict';

const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;
const Model = Sequelize.Model;

class TGUser extends Model {
  static init(sequelize) {
    return super.init({
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      firstName: {
        type: DataTypes.TEXT,
        field: 'first_name'
      },
      lastName: {
        type: DataTypes.TEXT,
        field: 'last_name'
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('current_timestamp'),
        field: 'created_at'
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('current_timestamp'),
        field: 'updated_at'
      }
    }, {
      sequelize,
      modelName: 'TGUser',
      tableName: 'tgusers'
    });
  }

  static associate(models) {
    models.TGUser.hasOne(models.TGState, {
      foreignKey: 'user_id' // column in target table
    });
  }
};

module.exports = TGUser;
