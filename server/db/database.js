const config = require('config');
const { Sequelize } = require('sequelize');

const TGUser = require('./models/tguser');
const TGState = require('./models/tgstate');

class Database {
  constructor(url) {
    this.url = url;
    this.orm = null;
    this.migrator = null;
    this.models = {};
  }

  setupModels() {
    this.models.TGUser = TGUser.init(this.orm);
    this.models.TGState = TGState.init(this.orm);
  }

  associateModels() {
    TGUser.associate(this.orm.models);
    TGState.associate(this.orm.models);
  }

  async connect(databaseUrl) {
    const orm = new Sequelize(databaseUrl, {
      dialect: 'postgres',
      dialectOptions: config.db.dialectOptions
    });
    await orm.authenticate();
    console.log('ORM connected');
    return orm;
  }

  async setupDatabase() {
    this.orm = await this.connect(this.url);
    this.setupModels();
    this.associateModels();
  }
}

module.exports = Database;
