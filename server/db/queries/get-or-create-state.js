const statuses = require('../../../common/statuses');

const getOrCreateState = async (db, user) => {
  const { TGState } = db.models;

  return await db.orm.transaction(async (t) => {
    const storedState = await TGState.findOne({
      where: { userId: user.id }
    }, { transaction: t });

    if (storedState != null) {
      return storedState;
    }

    const initialStateData = {
      userId: user.id,
      status: statuses.greetingScreen
    };
    const initialStateRecord = await TGState.create(initialStateData,
      { transaction: t });
    return initialStateRecord;
  });
};

module.exports = {
  getOrCreateState
};
