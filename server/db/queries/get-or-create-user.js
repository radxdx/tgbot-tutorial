const getOrCreateUser = async (db, ctx) => {
  const { TGUser } = db.models;

  return await db.orm.transaction(async (t) => {
    const candidate = await TGUser.findOne({
      where: { id: ctx.from.id }
    }, { transaction: t });

    if (candidate != null) {
      return candidate;
    }

    const userData = {
      id: ctx.from.id,
      firstName: ctx.from.first_name,
      lastName: ctx.from.last_name
    };
    const userRecord = await TGUser.create(userData, { transaction: t });
    return userRecord;
  });
};

module.exports = {
  getOrCreateUser
};
