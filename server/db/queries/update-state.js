const updateState = async (db, id, state) => {
  const { TGState } = db.models;

  return await db.orm.transaction(async (t) => {
    const currentStateRecord = await TGState.update({
      status: state
    },
    {
      where: { userId: id },
      transaction: t
    });
    return currentStateRecord;
  });
};

module.exports = {
  updateState
};
