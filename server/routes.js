// карта matching'а текущих состояний бота с контролерами (экранами)
const statuses = require('../common/statuses');
const controllers = require('./controllers');
const handlers = require('./handlers');

const routes = {
  [statuses.greeting]: controllers.greeting,
  [statuses.cmd1]: controllers.cmd1,
  [statuses.cmd2]: controllers.cmd2,
  [statuses.kb1]: controllers.kb1,
  [statuses.kb2]: controllers.kb2,
  [statuses.kb3]: controllers.kb3,
  [statuses.ikb1]: controllers.ikb1,
  [statuses.ikb2]: handlers.ikb2,
  [statuses.ikb3]: controllers.ikb3,
  [statuses.invalidInput]: controllers.invalidInput
};

module.exports = routes;
